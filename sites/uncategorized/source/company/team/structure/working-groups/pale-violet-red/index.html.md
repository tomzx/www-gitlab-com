---
layout: markdown_page
title: "PaleVioletRed Working Group"
description: ""
canonical_path: "/company/team/structure/working-groups/pale-violet-red/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Not Public

Certain aspects of this Working Group are [not public](https://about.gitlab.com/handbook/communication/#not-public).  The project code name PaleVioletRed has been selected to refer to the Working Group in accordance with [not public project naming conventions](https://about.gitlab.com/handbook/communication/#project-names).  Additional context can be found in [the WG agenda](https://docs.google.com/document/d/19-2QG0yXDt2p9vKLLxwrmqgBezk1Li7Zd6iTh-hotso/edit) (only accessible from within the company).

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | 2021-05-21 |
| Target End Date | 2021-12-03 |
| Slack           | #wg_pale-violet-red (only accessible from within the company) |
| Google Doc      | [Agenda](https://docs.google.com/document/d/19-2QG0yXDt2p9vKLLxwrmqgBezk1Li7Zd6iTh-hotso/edit) (only accessible from within the company) |

## Goals

This Working Group has the following goals:

1. Operate and develop the PaleVioletRed service
1. Track PaleVioletRed service workload and effectiveness
1. Transition the service to a long term owner


### Exit Criteria 

1. Successfully transition the service to a long term owner

### Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | Christopher Lefelhocz | VP Development            |
| Facilitator           | Sam Goldstein         | Director Engineering, Ops |
| Development Functional Lead  | Stan Hu | Engineering Fellow |
| Security Functional Lead    | TBD | |
| Infrastructure Functional Lead  | Igor Wiedler | Staff Site Reliability Engineer |
| Product Functional Lead  | Jackie Porter | Group Manager, Product, Verify |
| Member | Wayne Haber | Director Engineering |
| Member | Grzegorz Bizon | Staff Backend Engineer, Verify |
| Member | Joanna Shih | Quality Engineering Manager, Ops |
| Member | Charl de Wit | Security Manager, Trust and Safety |
| Member | Roger Ostrander | Senior Security Engineer, Abuse Operations |
| Member | Alex Groleau | Interim Manager, Security Automation |
